#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

int main(int argc, char *argv[]) {

    if (argc != 4) {
        cout << "Error: incorrect number of arguments " << endl;
        return -1;
    }

    ofstream outfile(argv[2]);
    if (!outfile) {
        cout << "Cannot open  output file;" << endl;
        return -1;
    }

    ifstream costfile(argv[3]);
    if (!costfile) {
        cout << "Cannot open cost file;" << endl;
        return -1;
    }

    /* store the cost in memory */
    int cost[10001];
    for (int c = 0; c < 10001; c++) {
        string line;
        while (getline(costfile, line)) {
            stringstream line_stream(line);
            int idx;
            line_stream >> idx;
            line_stream >> cost[idx];
        }
    }
    costfile.close();
/*
    cout << "cost 0: " << cost[0] << " = 0" << endl;
    cout << "cost 809: " << cost[809] << " = 16" << endl;
    cout << "cost 7309: " << cost[7309] << " = 29" << endl;
    cout << "cost 10000: " << cost[10000] << " = 11" << endl;
*/

/*reading input*/
    ifstream input(argv[1]);
    if (!input) {
        cout << "Cannot open  input file;" << endl;
        return -1;
    }

    int M;
    input >> M;

    // for each dataset
    for (int dataset = 0; dataset < M; dataset++) {
        int N, A, B;
        input >> N >> A >> B;
        cout << "[" << N << " " << A << " " << B << "]" << endl;
        // arrays to store the payment amounts
        // we could pay A or A + something, or A - something, thus the most we might pay is N*A (the whole amount in one period if B allows it)
        int P = N * A + 1;
        int possible_payments[P];

        for (int p = 0; p < P; p++) {
            possible_payments[p] = p;
        }

        // accumulated payments
        int payments_by_period[N]; // the payments for each period
        int Tp = 0; // sum of payments so far

        // for each period
        for (int k = 1; k <= N; k++) {
            int min_cost = cost[A * N];                     // initialize minimum cost to how much would be to pay all
            int max_amnt_hold_possible = Tp + A - k * A;    // how much less than A can we not pay
            cout << max_amnt_hold_possible << " " << B + A << endl;
            int pmnt = 0;
            int tmp_idx;
            int tmp_cost;
            int tmp_accrual;
            bool allowed;

            // Can we pay nothing with zero cost?
            tmp_idx = 0;

            tmp_cost = cost[possible_payments[tmp_idx]];
            tmp_accrual = possible_payments[tmp_idx] + Tp;
            allowed = (tmp_accrual >= k * A && tmp_accrual <= N * A);
            if (allowed) {
                if (min_cost > tmp_cost) {
                    min_cost = tmp_cost;
                    pmnt = possible_payments[tmp_idx];
                } else if (min_cost == tmp_cost && pmnt < possible_payments[tmp_idx]) {
                    pmnt = possible_payments[tmp_idx]; // also need to pay more if same cost!!!!
                }
            }

            cout << "--------------" << endl;
            // PAY EXACTLY A (when b=0) OR MORE THAN A: s + b for b = 0 .. B
            for (int b = 0; b <= B; b++) {
                tmp_idx = A + b;    // go higher in the possible payments array

                tmp_cost = cost[possible_payments[tmp_idx]];
                tmp_accrual = possible_payments[tmp_idx] + Tp;
                allowed = (tmp_accrual >= k * A && tmp_accrual <= N * A);
                if (allowed) {
                    if (min_cost > tmp_cost) {
                        min_cost = tmp_cost;
                        pmnt = possible_payments[tmp_idx];
                    } else if (min_cost == tmp_cost && pmnt < possible_payments[tmp_idx]) {
                        pmnt = possible_payments[tmp_idx]; // also need to pay more if same cost!!!!
                    }
                }
            }

            cout << "--------------" << endl;
            // PAY LESS THAN A: s - x for x = 0 .. Tp + A - k * A
            for (int x = 1; x <= max_amnt_hold_possible; x++) {
                tmp_idx = A - x;    // go lower in the possible payments array

                tmp_cost = cost[possible_payments[tmp_idx]];
                tmp_accrual = possible_payments[tmp_idx] + Tp;
                allowed = (tmp_accrual >= k * A && tmp_accrual <= N * A);
                if (allowed) {
                    if (min_cost > tmp_cost) {
                        min_cost = tmp_cost;
                        pmnt = possible_payments[tmp_idx];
                    } else if (min_cost == tmp_cost && pmnt < possible_payments[tmp_idx]) {
                        pmnt = possible_payments[tmp_idx]; // also need to pay more if same cost!!!!
                    }
                }
            }

            // payment made
            payments_by_period[k] = pmnt;
            Tp += pmnt;
            B = B + A - pmnt; // remove used balance


            cout << "we paid " << pmnt << " with cost of " << min_cost << endl;

        } // end period

        cout << "TOTAL PAID: " << Tp << " = " << N * A << " after " << N << " periods." << endl;
        cout << "==============================================" << endl;

    } // next dataset

    input.close();
    outfile.close();
    return 0;
}

